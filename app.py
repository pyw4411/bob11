from flask import Flask, request
app = Flask(__name__)

class Server_run():
   def home(self,username):
      return f"hello bob from {username}"

   def add(self,a,b):
      return str(a + b)

   def sub(self,a,b):
      return str(a - b)



Sr = Server_run()

@app.route('/',methods=["GET"])
def home():
   return Sr.home(request.args.get('username'))

@app.route('/add', methods=["GET"])
def add():
   a = int(request.args.get('a'))
   b = int(request.args.get('b'))
   return Sr.add(a,b)


@app.route('/sub', methods=["GET"])
def sub():
   a = int(request.args.get('a'))
   b = int(request.args.get('b'))
   return Sr.sub(a,b)



if __name__ == '__main__':

   app.run(host='0.0.0.0',port=8068)
